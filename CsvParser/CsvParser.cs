﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace Qifun.CsvParser
{
    internal class CsvParser
    {
        enum State
        {
            START,
            DELIMITER,
            FIELD,
            END,
            QUOTESTART,
            QUOTEEND,
            QUOTEFIELD,
        }

        private readonly char escapedChar;
        private readonly char delimiter;
        private readonly char quoteChar;

        internal CsvParser(char escapedChar, char delimiter, char quoteChar)
        {
            this.escapedChar = escapedChar;
            this.delimiter = delimiter;
            this.quoteChar = quoteChar;
        }

        private static string[] Parse(string input, char escapedChar, char delimiter, char quoteChar)
        {
            var buffer = input.ToCharArray();
            var fields = new LinkedList<string>();
            var field = new StringBuilder();
            State state = State.START;
            int pos = 0;
            int bufferLength = buffer.Length;

            while (state != State.END && pos < bufferLength)
            {
                var current = buffer[pos];
                switch (state)
                {
                    case State.START:
                    case State.DELIMITER:
                        if (current == quoteChar)
                        {
                            state = State.QUOTESTART;
                            ++pos;
                        }
                        else if (current == delimiter)
                        {
                            fields.AddLast(field.ToString());
                            field = new StringBuilder();
                            state = State.DELIMITER;
                            ++pos;
                        }
                        else if (current == '\n' || current == '\u2028' || current == '\u2029' || current == '\u0085')
                        {
                            fields.AddLast(field.ToString());
                            field = new StringBuilder();
                            state = State.END;
                            ++pos;
                        }
                        else if (current == '\r')
                        {
                            if (pos + 1 < bufferLength && buffer[1] == '\n')
                            {
                                ++pos;
                            }
                            fields.AddLast(field.ToString());
                            field = new StringBuilder();
                            state = State.END;
                            ++pos;
                        }
                        else
                        {
                            field.Append(current);
                            state = State.FIELD;
                            ++pos;
                        }
                        break;
                    case State.FIELD:
                        if (current == escapedChar)
                        {
                            if (pos + 1 < bufferLength)
                            {
                                if (buffer[pos + 1] == escapedChar || buffer[pos + 1] == delimiter)
                                {
                                    field.Append(buffer[pos + 1]);
                                    state = State.FIELD;
                                    pos += 2;
                                }
                                else
                                {
                                    throw new Exception("parser error");
                                }
                            }
                            else
                            {
                                state = State.QUOTEEND;
                                ++pos;
                            }
                        }
                        else if (current == delimiter)
                        {
                            fields.AddLast(field.ToString());
                            field = new StringBuilder();
                            state = State.DELIMITER;
                            ++pos;
                        }
                        else if (current == '\n' || current == '\u2028' || current == '\u2029' || current == '\u0085')
                        {
                            fields.AddLast(field.ToString());
                            field = new StringBuilder();
                            state = State.END;
                            ++pos;
                        }
                        else if (current == '\r')
                        {
                            if (pos + 1 < bufferLength && buffer[1] == '\n')
                            {
                                ++pos;
                            }
                            fields.AddLast(field.ToString());
                            field = new StringBuilder();
                            state = State.END;
                            ++pos;
                        }
                        else
                        {
                            field.Append(current);
                            state = State.FIELD;
                            ++pos;
                        }
                        break;
                    case State.QUOTESTART:
                        if (current == quoteChar)
                        {
                            if (pos + 1 < bufferLength && buffer[pos + 1] == quoteChar)
                            {
                                field.Append(quoteChar);
                                state = State.QUOTEFIELD;
                                pos += 2;
                            }
                            else
                            {
                                state = State.QUOTEFIELD;
                                ++pos;
                            }
                        }
                        else
                        {
                            field.Append(current);
                            state = State.QUOTEFIELD;
                            ++pos;
                        }
                        break;
                    case State.QUOTEEND:
                        if (current == delimiter)
                        {
                            fields.AddLast(field.ToString());
                            field = new StringBuilder();
                            state = State.DELIMITER;
                            ++pos;
                        }
                        else if (current == '\n' || current == '\u2028' || current == '\u2029' || current == '\u0085')
                        {
                            fields.AddLast(field.ToString());
                            field = new StringBuilder();
                            state = State.END;
                            ++pos;
                        }
                        else if (current == '\r')
                        {
                            if (pos + 1 < bufferLength && buffer[1] == '\n')
                            {
                                ++pos;
                            }
                            fields.AddLast(field.ToString());
                            field = new StringBuilder();
                            state = State.END;
                            ++pos;
                        }
                        else
                        {
                            throw new Exception("parser error");
                        }
                        break;
                    case State.QUOTEFIELD:
                        if (current == escapedChar && escapedChar != quoteChar)
                        {
                            if (pos + 1 < bufferLength)
                            {
                                if (buffer[pos + 1] == escapedChar || buffer[pos + 1] == quoteChar)
                                {
                                    field.Append(buffer[pos + 1]);
                                    state = State.QUOTEFIELD;
                                    pos += 2;
                                }
                                else
                                {
                                    throw new Exception("parser error");
                                }
                            }
                            else
                            {
                                throw new Exception("parser error");
                            }
                        }
                        else if (current == quoteChar)
                        {
                            if (pos + 1 < bufferLength && buffer[pos + 1] == quoteChar)
                            {
                                field.Append(quoteChar);
                                state = State.QUOTEFIELD;
                                pos += 2;
                            }
                            else
                            {
                                state = State.QUOTEEND;
                                ++pos;
                            }
                        }
                        else
                        {
                            field.Append(current);
                            state = State.QUOTEFIELD;
                            ++pos;
                        }
                        break;
                    case State.END:
                        throw new Exception("unreachable code");
                }
            }

            switch (state)
            {
                case State.DELIMITER:
                    fields.AddLast("");
                    return fields.ToArray();
                case State.QUOTEFIELD:
                    return null;
                default:
                    if (field.Length != 0)
                    {
                        switch (state)
                        {
                            case State.FIELD:
                            case State.QUOTEEND:
                                fields.AddLast(field.ToString());
                                break;
                            default: break;
                        }
                    }
                    return fields.ToArray();
            }
        }

        internal string[] ParseLine(string input)
        {
            return Parse(input, escapedChar, delimiter, quoteChar);
        }



    }
}
