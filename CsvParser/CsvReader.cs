﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Qifun.CsvParser
{
    public class CsvReader : IEnumerable<string[]>
    {
        private const char Delimiter = ',';
        private const char QuoteChar = '"';
        private const char EscapeChar = '"';

        private CsvParser parser;
        private LineReader lineReader;
        private TextReader reader;

        public CsvReader(TextReader reader)
        {
            parser = new CsvParser(EscapeChar, Delimiter, QuoteChar);
            lineReader = new LineReader(reader);
            this.reader = reader;
        }


        public void Close()
        {
            lineReader.Close();
            reader.Close();
        }

        public string[] ReadNext()
        {
            string leftOver = null;
            while (true)
            {
                var nextLine = lineReader.ReadLine();
                if (nextLine == null)
                {
                    if (leftOver == null)
                    {
                        return null;
                    }
                    else
                    {
                        throw new Exception("incomplete line");
                    }
                }
                else
                {
                    string line;
                    if (leftOver == null)
                    {
                        line = nextLine;
                    }
                    else
                    {
                        line = leftOver + nextLine;
                    }
                    string[] parsedResult = parser.ParseLine(line);
                    if (parsedResult != null)
                    {
                        return parsedResult;
                    }
                    else
                    {
                        leftOver = line;
                    }
                }
            }

        }

        public static CsvReader Factory(FileInfo fileInfo)
        {
            return Factory(fileInfo.OpenRead());
        }

        public static CsvReader Factory(Stream stream)
        {
            return Factory(stream, new UTF8Encoding());
        }

        public static CsvReader Factory(string str)
        {
            var stringReader = new StringReader(str);
            try
            {
                return new CsvReader(stringReader);
            }
            catch (Exception e)
            {
                stringReader.Close();
                throw e;
            }
        }

        public static CsvReader Factory(Stream stream, Encoding encoding)
        {
            var streamReader = new StreamReader(stream, encoding);
            try
            {
                return new CsvReader(streamReader);
            }
            catch (Exception e)
            {
                streamReader.Close();
                throw e;
            }
        }

        public IEnumerator<string[]> GetEnumerator()
        {
            var line = ReadNext();
            while (line != null)
            {
                yield return line;
                line = ReadNext();
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
             return GetEnumerator();
        }



    }

}
