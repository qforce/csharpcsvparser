﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qifun.CsvParser;
using System.Collections;
using Qifun.CsvParserUtil;
namespace CsvParserUtilTest
{
    [TestClass]
    public class CsvAsMapText
    {
        [TestMethod]
        public void TestMapGet()
        {
            var csvReader = CsvReader.Factory(new FileInfo("../../武将配置表.csv"));
            var csvMap = new CsvAsMap(csvReader);
            Assert.AreEqual("关羽", csvMap["2", "武将名称"]);
            Assert.AreEqual("200字", csvMap["1", "简介"]);
            Assert.AreEqual("1", csvMap["2", "技能1参数3"]);
            csvReader.Close();
        }
        [TestMethod]
        public void TestMapGetLast()
        {
            var csvReader = CsvReader.Factory(new FileInfo("../../test2.csv"));
            var csvMap = new CsvAsMap(csvReader);
            Assert.AreEqual("", csvMap["GuaiWu6", "技能3"]);
            Assert.AreEqual("NanZhanShiSpSkill", csvMap["GuaiWu6", "大招"]);
            csvReader.Close();
        }
        [TestMethod]
        public void TestMapError()
        {
            var csvReader = CsvReader.Factory(new FileInfo("../../武将配置表.csv"));
            var csvMap = new CsvAsMap(csvReader);
            try
            {
                var tryNow = csvMap["3", "简介"];
            }
            catch (Exception exce)
            {
                Assert.AreEqual(exce.Message, "参数输入错误，无法找到相应的值！");
            }
            csvReader.Close();
        }
        [TestMethod]
        public void TestMapNameError()
        {
            try
            {
                var csvReader = CsvReader.Factory(new FileInfo("../../武将配置表2.csv"));
                var csvMap = new CsvAsMap(csvReader);
                csvReader.Close();
            }
            catch (Exception exce)
            {
                Assert.AreEqual(exce.Message, "列名重复，重复的列名有: 统帅 武力 智力");
            }
        }
    }

}
