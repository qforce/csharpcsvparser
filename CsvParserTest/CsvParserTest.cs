﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Qifun.CsvParser;

namespace Qifun.CsvParserTest
{
    [TestClass]
    public class CsvParserTest1
    {
        [TestMethod]
        public void TestParser()
        {
            var csvReader = CsvReader.Factory(new FileInfo("../../test.csv"));
            var enumerator = csvReader.GetEnumerator();
            string[] line1 = { "a", "b", "c,c", "d" };
            string[] line2 = { "1", "2", "3\n3", "4" };
            enumerator.MoveNext();
            string[] parsedLine1 = enumerator.Current;
            enumerator.MoveNext();
            string[] parsedLine2 = enumerator.Current;
            for (int i = 0; i < 4; i++)
            {
                Assert.AreEqual(line1[i], parsedLine1[i]);
                Assert.AreEqual(line2[i], parsedLine2[i]);
            }
            csvReader.Close();
        }
    }
}
